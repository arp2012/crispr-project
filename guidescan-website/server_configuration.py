__author__ = 'Alex Perez and Yuri Pritykin'

#####################
#                   #
#   Introduction    #
#                   #
#####################

print 'This generates a configuration file for server.py'

#################
#               #
#   Libraries   #
#               #
#################

import ConfigParser
import os

#####################
#                   #
#   Configuration   #
#                   #
#####################

#set configuration settings
current_dir = os.path.dirname(os.path.abspath(__file__))
config = ConfigParser.RawConfigParser()

#local section configurations
config.add_section('/')
config.set('/','tools.staticdir.on',True)
config.set('/','tools.staticdir.dir',current_dir + '/static/')
config.set('/','tools.secureheaders.on',True)

#global section configurations
config.add_section('global')
config.set('global','server.environment','production')
config.set('global','engine.autoreload.on',True)
config.set('global','engine.autoreload.frequency',5)
config.set('global','server.socket_host', '127.0.0.1')
config.set('global','server.socket_port',8080)
config.set('global','server.thread_pool',30)
config.set('global','engine.timeout_monitor.on',False)
config.set('global','log.access_file','')
config.set('global','log.error_file','')
config.set('global','request.show_tracebacks',False)

#write out configuration file
with open('config.cfg','wb') as configfile:
    config.write(configfile)
