Below packages needed for running the server.py script:

Python 2.7
zlib-dev (Can be installed using `apt-get install zlib-dev` or `yum install zlib-devel`)


Python packages that should be included with python 2.7
```
StringIO
csv
os.path
threading
string
math
time
re
subprocess
```

Python packages that should be installed:
```
numpy
cherrypy
jinja2
Biopython
logging
ipdb
xlwt
pysam
psutil
fastinterval
```

The above packages can be installed using `pip install numpy cherrypy jinja2 Biopython logging ipdb xlwt pysam psutil fastinterval`. 

Once installed, run the website using "python server.py". The server can then be accessed through localhost address "127.0.0.1:8080". 
