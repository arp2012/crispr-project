__author__ = 'Alex Perez, Yuri Pritykin, Sagar Chhangawala'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""This is the server script for the GuideScan website
"""

#################
#               #
#   Libraries   #
#               #
#################

import ast
import csv
import os
import os.path
current_dir = os.path.dirname(os.path.abspath(__file__))
#current_dir = '/Users/Xerez/Projects/Ventura/CRISPR_algorithm/Yuri_version/annotation/code/iguide-crispr/iguide-website'
import cgi
import cgitb; cgitb.enable()
import string
import time
import logging
import tempfile
from logging.handlers import TimedRotatingFileHandler
import re
import cProfile, pstats, StringIO
from collections import defaultdict

import cherrypy
from cherrypy.lib.static import serve_file
from jinja2 import Environment, FileSystemLoader

from guidescan import guidequery
from guidescan import util

#########################
#                       #
#   Load Environment    #
#                       #
#########################

env = Environment(loader=FileSystemLoader('templates'))

#############################
#                           #
#   Auxillary Functions     #
#                           #
#############################

def noBodyProcess():
    """direct control of file upload destination"""
    cherrypy.request.process_request_body = False

def expose_as_csv(f):
    """
    awesome trick to expose data as csv

    taken from:
    http://code.activestate.com/recipes/573461-publish-list-data-as-csv-file/
    """

    @cherrypy.expose()
    #@strongly_expire
    def wrap(*args, **kw):
        rows = f(*args, **kw)
        out = StringIO.StringIO()
        writer = csv.writer(out) 
        writer.writerows(rows)
        cherrypy.response.headers["Content-Type"] = "application/csv" #invokes download
        cherrypy.response.headers["Content-Length"] = out.len
        return out.getvalue()
    return wrap

def secureheaders():
    headers = cherrypy.response.headers
    headers['X-Frame-Options'] = 'DENY'
    headers['X-XSS-Protection'] = '1; mode=block'
    headers['Content-Security-Policy'] = "default-src='self'"

def midnightloghandler(fn):
    h = TimedRotatingFileHandler(fn, "midnight")
    h.setLevel(logging.DEBUG)
    h.setFormatter(cherrypy._cplogging.logfmt)
    return h

#################################
#                               #
#   Annotation Interval Trees   #
#                               #
#################################

SUPPORTED_ANNOTS = ['hg38', 'mm10', 'dm6', 'sacCer3']
def load_annots():
    annots = {}
    for tag in SUPPORTED_ANNOTS:
        annots[tag] = util.create_annot_inttree(tag)
    return annots
ANNOTS = load_annots()

BAMDATA = {'dm6': '../testing-data/website-test-bam/dm6_website_chr4_310000-320000.bam',
           'hg38': '../testing-data/website-test-bam/hg38_website_chr4_310000_320000.bam',
           'mm10': '../testing-data/website-test-bam/mm10_website_chr4_3100000_3200000.bam',
           }

#################
#               #
#   Classes     #
#               #
#################

class Crispr:
    def __init__(self,queries,genometag,mode,flank_size,ordering,display,datafile,outdir=''):
        """Main class to process user input and prepare results to show.

        queries: (str) user input in query text area; supposedly contains
                 one or multiple genomic region coordinates to query
                 the database;
                 TODO: allow gene names as queries here
        genometag: str with short name of genome (e.g., "hg38")
        mode: "flanking" or "within"
        flank_size: (int) size of flanking window (for mode "flanking")
        ordering: "coordinates" or "offtargets" or "score",
                  determines how output guideRNAs are sorted
        """
        self.queries = queries
        self.genometag = genometag
        # name of BAM file with guideRNA database
        self.bamdata = BAMDATA.get(self.genometag) 
        self.mode = mode
        self.error = []      # where all errors will be stored
        self.results = []    # list of pairs
                             # (query, mode, bedlines of guideRNAs)
                             # where mode is "within" or "upstream"
                             # or "downstream"
        self.flank_size = flank_size
        self.ordering = ordering
        self.display = display
        self.offtargets = defaultdict(list)  # dict of the form
                              # {sequence of guideRNA + PAM :
                              #  bedlines of offtargets for this guideRNA})
                              # with all off-targets across all queries
                              # combined
        self.datafile = datafile
        self.outdir = outdir

    def calculate(self):
        # TODO: restructure all error messsages
        # TODO: add check that self.bamdata is present and exists

        queries = self.process_input(self.queries)

        # if not self.kmer.isdigit():
        #     print("Kmer not a digit "+self.kmer)
        #     self.error.append("ERROR")
        #     self.error.append("Please enter a positive integer in the \"Kmer\" field.")
        # else:
        #     for i in range(len(queries)):
        #         query_type = self.getType(queries[i])
        #         print queries[i], query_type

        #         if not self.kmer.isdigit():
        #             self.error.append([queries[i],"blank.gif", "ERROR", ["Please enter a positive integer in the \"Kmer\" field."], 0, "", []])
        #         elif not self.flank_size.isdigit():
        #             self.error.append([queries[i],"blank.gif", "ERROR", ["Please enter a positive integer in the \"Flanking\" field."], 0, "", []])
        #         elif query_type == 'ezseq':
        #             self.show_results_ezseq(queries[i])
        #         else:
        #             self.error.append([queries[i],"", "ERROR", "Input type could not be identified. Please ensure that you entered a valid coordinate in format: chrX:120050-120100"])
        print("inside calculate ",self.datafile)

        #If batch mode 
        if self.datafile:
          print("found datafile!")
          print(self.datafile,self.bamdata,self.mode)
          self.outdir=tempfile.mkdtemp()
          guidequery.batch_query(input_file=self.datafile,output_dir=self.outdir,bam_file=self.bamdata,target=self.mode,
                                    flankdistance=self.flank_size,select=self.display,sort=self.ordering,n=3,formating='csv',off=False)
          print("finished guidequery")

        #or continue with normal query
        else:
          for i in range(len(queries)):
              query_type = self.getType(queries[i])
              print queries[i], query_type
              if not self.flank_size.isdigit():
                  self.error.append([queries[i],"blank.gif", "ERROR", ["Please enter a positive integer in the \"Flanking\" field."], 0, "", []])
              elif query_type == 'ezseq':
                  self.show_results_ezseq(queries[i])
              else:
                  self.error.append([queries[i],"", "ERROR", "Input type could not be identified. Please ensure that you entered a valid coordinate in format: chrX:120050-120100"])
    
    def process_input(self, inputs):
        splitInput = inputs.splitlines()
        splitInput = [s.strip() for s in splitInput]
        return splitInput
       
    def getType(self, seq):
        seqType = None
        if re.match("^chr[0-9|XYM]+:[0-9]+-[0-9]+$", seq) is not None:
            seqType = "ezseq"
        if re.match("^chr[[23][LR]|4XYM]+:[0-9]+-[0-9]+$",seq) is not None:
            seqType = "ezseq"
        return seqType
    
    def show_results_ezseq(self, query):
        if self.mode == "flanking":
            query_split = query.strip().split(':')
            query_split = (query_split[0],) + tuple(int(x) for x in query_split[1].split('-'))
            chrom,start,end = query_split

            if(start-int(self.flank_size) > 0):
                flank_start=start-int(self.flank_size)
            else:
                flank_start=0

            flank_end=end+int(self.flank_size)

            query1=str(chrom)+":"+str(flank_start)+"-"+str(start)
            query2=str(chrom)+":"+str(end)+"-"+str(flank_end)

            guides1 = guidequery.query_bam(self.bamdata,str(query1),
                                           offcoords=True,
                                           onebased=False)
            guides2 = guidequery.query_bam(self.bamdata,str(query2),
                                           offcoords=True,
                                           onebased=False)

            if self.display == 'top3':
                guides1,guides2 = guidequery.sgRNA_automatic_selection(guides1, selection=self.ordering,n=3),\
                                  guidequery.sgRNA_automatic_selection(guides2, selection=self.ordering,n=3)

            else:
                guides1,guides2 = guidequery.sort_guides(guides1, self.ordering),\
                                  guidequery.sort_guides(guides2, self.ordering)

            bedlines1, bedlines2 = guidequery.get_bed_lines(guides1, offtargets=True), \
                                   guidequery.get_bed_lines(guides2, offtargets=True)

            annots = ANNOTS.get(self.genometag)
            if annots:
                if bedlines1 and bedlines2:
                    bedlines1,bedlines2 = guidequery.annotate_bed(bedlines1, annots),\
                                          guidequery.annotate_bed(bedlines2, annots)
                elif bedlines1:
                    bedlines1 = guidequery.annotate_bed(bedlines1, annots)
                elif bedlines2:
                    bedlines2 = guidequery.annotate_bed(bedlines2, annots)
                else:
                    pass
            """
            bedlines_flank = [bedlines1,bedlines2]
            if self.ordering == 'offtargets':
                for i in bedlines_flank:
                    i.sort(key=lambda x:
                           int(x[6] if util.is_number(x[6]) else -1))

            if self.ordering == 'score':
                for i in bedlines_flank:
                    i.sort(key=lambda x:
                           float(x[4] if util.is_number(x[4]) else -1),
                           reverse=True)
            """
            self.results.append((query, 'upstream',
                                [b for b in bedlines1 if b[9] == 0]))
            self.results.append((query, 'downstream',
                                 [b for b in bedlines2 if b[9] == 0]))
            bedlines_flank = [bedlines1, bedlines2]
            for j in bedlines_flank:
                for b in j:
                    self.offtargets[b[3]].append(b)

        elif self.mode == "within":
            guides = guidequery.query_bam(self.bamdata, str(query),
                                          offcoords=True,
                                          onebased=False)

            if self.display== 'top3':
                guides = guidequery.sgRNA_automatic_selection(guides, selection=self.ordering,n=3)
            else:
                guides = guidequery.sort_guides(guides,self.ordering)

            bedlines = guidequery.get_bed_lines(guides, offtargets=True)

            annots = ANNOTS.get(self.genometag)
            if annots:
                bedlines = guidequery.annotate_bed(bedlines, annots)

            """
            if self.ordering == 'offtargets':
                bedlines.sort(key=lambda x:
                              int(x[6] if util.is_number(x[6]) else -1))

            if self.ordering == 'score':
                bedlines.sort(key=lambda x:
                              float(x[4] if util.is_number(x[4]) else -1),
                              reverse=True)
            """

            self.results.append((query, "within",
                                 [b for b in bedlines if b[9] == 0]))
            for b in bedlines:
                self.offtargets[b[3]].append(b)

class TemporaryFileStorage(cherrypy._cpreqbody.Part):
    """custom entity part that will always create a named temporary file for the entities (stackoverflow)"""
    maxrambytes = 0

    def make_file(self):
        return tempfile.NamedTemporaryFile()

class Root:
    def __init__(self):
        print('Loading Site...')

    @cherrypy.expose
    def index(self, **params):
        """
        set up site
        """

        tmpl = env.get_template('crispr.html')
        queries = "chr4:310000-320000\n"
        self.kmer = "20"
        self.genome = "hg38"
        self.mode="within"
        self.flank_size="1000"
        self.ordering='offtargets'
        self.display='all'
        self.datafile=''

        acceptable_characters = string.ascii_letters + string.digits + string.whitespace + '#:-_.*'
        queries = ''.join(character for character in queries if character in acceptable_characters)
        self.kmer = ''.join(character for character in self.kmer if character in acceptable_characters)
        self.genome = ''.join(character for character in self.genome if character in acceptable_characters)
        self.mode = ''.join(character for character in self.mode if character in acceptable_characters)
        self.flank_size = ''.join(character for character in self.flank_size if character in acceptable_characters)
        self.ordering = ''.join(character for character in self.ordering if character in acceptable_characters)
        self.display = ''.join(character for character in self.display if character in acceptable_characters)
        
        return tmpl.render(queries=queries, kmer = self.kmer,
                           genome = self.genome,
                           mode=self.mode, flank_size=self.flank_size,
                           ordering=self.ordering, display=self.display, datafile=self.datafile)

    @cherrypy.expose
    def show_results(self,datafile=None,queries="", kmer="20", genome="hg38",
                     mode="within", flank_size=1000, ordering='offtargets',display='all',
                     **params):

        tmpl = env.get_template('crispr.html')


        crispr = Crispr(queries = queries.replace(',', ''),
                        genometag = genome, mode = mode,
                        flank_size = flank_size, ordering = ordering,display=display,datafile='')
        crispr.calculate()
        cherrypy.log(cherrypy.request.remote.ip + "\t" + kmer + "\t" + str(len(crispr.results)) + " results")
        return tmpl.render(error=crispr.error,
                           result=crispr.results, offtargets=crispr.offtargets,
                           queries=crispr.queries,
                           kmer=kmer, genome=crispr.genometag,
                           mode=crispr.mode, flank_size=crispr.flank_size,
                           ordering=crispr.ordering,display=crispr.display)
   
    @cherrypy.expose
    def show_offtargets(self, offtargets="", guiderna="", **params):
        tmpl = env.get_template('offtargets.html')
        offtargets = ast.literal_eval(offtargets)
        return tmpl.render(offtargets=offtargets, guiderna=guiderna)

    @expose_as_csv
    def GuideScan_results_csv(self, result=[], kmer="", genome="",region="",
                             flank_size="",**params):
        """
        expose predictions as comma separated value file
        """
        # TODO: this function should be updated to produce meaningful csv
        timestamp = "Generated " + time.strftime("%Y/%m/%d %H:%M:%S")
        
        csv = [[timestamp], ["Genome","Kmer Length","User Input Coordinate","gRNA","Coordinates","Off Targets","Mismatch Tag","Warnings"]]    #header
        result = ast.literal_eval(result)

        for item in result:
            #If there is an error, write it out to the file.
            if len(item) > 2:
                csv.append([genome]+ [kmer] + [item[0]] + ['','',''] +[item[2] + " " +item[3]])
            else:
                results = item[1]
                print results
                for entry in results:
                    csv.append([genome]+ [kmer] + [item[0]] + list(entry) + [''])
        return csv

    @cherrypy.expose()
    def upload(self,datafile=None,queries="", kmer="20", genome="hg38",
                     mode="within",flank_size=500,ordering='score',display='',
                     **params):

        df=tempfile.NamedTemporaryFile(mode='w',delete=False)
        df.write(datafile.file.read())
        df.close()

        tmpl = env.get_template('crispr.html')

        print(df.name)

        crispr = Crispr(queries = queries.replace(',', ''),
                        genometag = genome, mode = mode,
                        flank_size = flank_size,ordering = ordering,display=display,datafile=df.name)
        crispr.calculate()
        cherrypy.log(cherrypy.request.remote.ip + "\t" + kmer + "\t" + str(len(crispr.results)) + " results")

        if os.path.exists(df.name):
            print('%s EXISTS! ' % (df.name))

        if os.path.exists(crispr.outdir):
            print('crispr.outdir is %s' % (crispr.outdir))

        if os.path.exists(crispr.outdir + '/GuideScan_batch_output.csv'):
            print('GuideScan_batch_output.csv EXISTS')
        else:
            os.listdir(crispr.outdir)

        file_to_download = ('%s%s' % (crispr.outdir,'/GuideScan_batch_output.csv'))
        serve_file(file_to_download,"application/x-download","attachment")

        #os.remove(df.name)
        #os.remove(crispr.outdir)

        return tmpl.render(queries=queries, kmer = 20,
                           genome = genome,
                           mode=crispr.mode, flank_size=crispr.flank_size,
                           ordering=crispr.ordering, display=crispr.display, datafile=crispr.datafile)
        #print(os.path.exists(df.name))
        #print(os.path.exists(outdir + '/GuideScan_batch_output.csv'))

    @cherrypy.expose()
    def help(self, **params):
        tmpl_help = env.get_template('help.html')
        return tmpl_help.render()

    @cherrypy.expose()
    def contact(self, **params):
        tmpl_contact = env.get_template('contact.html')
        return tmpl_contact.render()

"""

class Download():

    def __init__(self):
        print('download called')

    @cherrypy.expose()
    def download(self,filepath):
        return serve_file(filepath,"application/x-download","attachment")
"""

#####################
#                   #
#   Core Function   #
#                   #
#####################

cherrypy.tools.secureheaders = cherrypy.Tool('before_finalize', secureheaders, priority=60)
cherrypy.tools.noBodyProcess = cherrypy.Tool('before_request_body',noBodyProcess)
cherrypy.server.socket_timeout = 300

#####################
#   Configuration   #
#####################

config = {'/':

                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': current_dir + "/static/",
                 'tools.secureheaders.on': True,
                 'request.body.part_class':TemporaryFileStorage
                },

       'global':    
                { 'server.environment': 'production',
                  'engine.autoreload.on': True,
                  'engine.autoreload.frequency': 5,
                  'server.socket_host': '127.0.0.1',
                  'server.socket_port': 8080,
                  'server.thread_pool': 30,
                  'engine.timeout_monitor.on': False,
                  'log.access_file': "",
                  'log.error_file': "",
                  'request.show_tracebacks':False
                },
        }

error_dirname = current_dir + "/logs/error"
access_dirname = current_dir + "/logs/access"
if not os.path.exists(error_dirname):
    os.makedirs(error_dirname)
if not os.path.exists(access_dirname):
    os.makedirs(access_dirname)

log = cherrypy.log 
log.error_file = ""
log.access_file = "" 

for handler in log.error_log.handlers:
    log.error_log.removeHandler(handler)
for handler in log.access_log.handlers:
    log.access_log.removeHandler(handler)

log.error_log.addHandler(midnightloghandler(error_dirname + "/log_error")) 
log.access_log.addHandler(midnightloghandler(access_dirname + "/log_access")) 

cherrypy.quickstart(Root(), config=config)
