#!/bin/bash

set -e

pinger_script="/guidescan/crispr-project/guidescan-website/pinger.py"
site_url="guidescan.com"

python $pinger_script -s $site_url
