#!/bin/bash

set -e
timestamp=$(date +%s)
script="/usr/bin/python /guidescan/crispr-project/guidescan-website/server.py"

screen -S 'DONTKILL_'$timestamp -d -m bash -c 'cd /guidescan/crispr-project/guidescan-website/; sudo /usr/bin/python /guidescan/crispr-project/guidescan-website/server.py'
