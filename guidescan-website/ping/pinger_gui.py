__author__ = 'Alexendar Perez & Sagar Chhanagawala & Yuri Pritykin'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""pings GuideScan site and if it determines it is down this script notifies admin"""

#################
#               #
#   Libraries   #
#               #
#################

import sys
import argparse
import subprocess

import easygui
from time import gmtime, strftime

#########################
#                       #
#   Auxillary Function  #
#                       #
#########################

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--site_url',help='URL of site to ping',required=True)

    args = parser.parse_args()
    site = args.site_url

    return site

def site_check_and_respawn(site):
    """checks site to ensure it is contactable. If it is not then the script attempts to respawn site

    Inputs:

    """
    cmd1 = 'curl -Is %s | head -1' % (site)
    check = subprocess.check_output(cmd1, shell=True)
    if check:
        sys.stdout.write('%s was contactable at %s \n' % (site, strftime("%Y-%m-%d %H:%M:%S", gmtime())))
    else:
        sys.stdout.write('%s was NOT contactable at %s \n' % (
        site, strftime("%Y-%m-%d %H:%M:%S", gmtime())))
        easygui.msgbox("GuideScan not contactable! Check it out", title="GuideScan Alert")

    sys.stdout.write('site check complete \n')

#####################
#                   #
#   Main Function   #
#                   #
#####################

def main():
    """
    site = 'guidescan.com'
    site = 'guidescan.mskcc.org'
    """

    #user input
    site = arg_parser()

    #ping site
    site_check_and_respawn(site)

if __name__ == '__main__':
    main()

