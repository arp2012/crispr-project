__author__ = 'Alex Perez, Yuri Pritykin, Sagar Chhangawala'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""This is the server script for the GuideScan website
"""

#################
#               #
#   Libraries   #
#               #
#################

import ast
import csv
import os
import os.path
current_dir = os.path.dirname(os.path.abspath(__file__))
#current_dir = '/Users/Xerez/Projects/Ventura/CRISPR_algorithm/Yuri_version/annotation/code/iguide-crispr/iguide-website'
import cgi
import cgitb; cgitb.enable()
import string
import time
import logging
import itertools
import tempfile
import pysam
from logging.handlers import TimedRotatingFileHandler
import re
import os
import sys
import gc
import cProfile, pstats, StringIO
from collections import defaultdict
from cherrypy.lib.static import serve_file
from cherrypy.lib.static import staticfile
from cherrypy.lib.static import serve_download

import cherrypy
from jinja2 import Environment, FileSystemLoader

from guidescan import guidequery
from guidescan import util

#########################
#                       #
#   Load Environment    #
#                       #
#########################

env = Environment(loader=FileSystemLoader('templates'), cache_size=0)

#############################
#                           #
#   Auxillary Functions     #
#                           #
#############################

def blat_processing(blat, fasta, infile, outdir, verbose=True):
    """

    :param blat: absolute filepath to blat binary, (unix command line: which blat)
    :param fasta: absolute filepath to indexed fasta file
    :param infile: input file with sequences to process; fasta format
    :param outdir: absolute filepath to output directory
    :param verbose: return stdout
    :return: returns absolute filepath of blat coordinates file

    """
    blat_check = blat_local_installation_verification(blat)

    if blat_check == 0:
        out = '%s/out_for_blat.txt' % (outdir)
        run_blat(blat, fasta, infile, out)
        outfile = blat_coordinate_file(out, verbose)
        os.remove(out)
        return outfile
    else:
        sys.stderr.write('blat not run on %s due to blat not being locally installed on system\n' % test_in)
        return

def blat_coordinate_file(out,verbose=False):
    """generates a tab-delimited file with genomic coordinates for GuideScan

    Input:
    out: first output object of run_blat()
    verbose: write out lines in blat file

    Output:
    a tab-delimited file with the best blat estimate of the genomic coordinates for the input sequences

    Note:
    this function will only return coordinates for a sequence, if that sequence has a perfect alignment, according to BLAT,
    in a given genome.

    """

    outdir = '/'.join(out.split('/')[:-1])
    with open('%s/blat_coordinates.txt' % (outdir),'w') as outfile:
        with open(out,'r') as infile:
            for line in infile:
                clean_line = line.lstrip().rstrip()
                parts = clean_line.split()
                if verbose == True:
                    print clean_line
                else:
                    pass
                if parts:
                    try:
                        score,qsize = int(parts[0]),int(parts[10])
                        if score == qsize:
                            blat_coordinate = '%s:%s-%s' % (parts[13], parts[15], parts[16])
                            outfile.write('%s\n' % blat_coordinate)
                    except ValueError:
                        continue

            sys.stdout.write('coordinates file written to %s/blat_coordinates.txt\n' % outdir)

    return '%s/blat_coordinates.txt' % (outdir)

def run_blat(blat,fasta,infile,outfile):
    """executes blat command

    Input:
    blat: absolute filepath of blat
    fasta: absolute filepath to indexed fasta file for organism under consideration
    infile: input fasta file
    outfile: output fasta file

    Output:
    blat_file: output file from blat

    """
    cmd = '%s %s %s %s' % (blat,fasta,infile,outfile)
    os.system(cmd)
    sys.stdout.write('blat run complete\noutput written to %s\n' % (outfile))

def blat_local_installation_verification(blat):
    """verifies that blat is locally installed on system

    Inputs:
    blat: absolute filepath of blat

    Outputs:
    returns 0 if blat is locally installed and 1 if not

    """
    ucsc_executables = 'http://hgdownload.soe.ucsc.edu/admin/exe/'
    try:
        check = os.system(blat)
        if check == 65280:
            sys.stdout.write('blat is locally installed\n')
            return 0
        else:
            sys.stderr.write('blat is not locally installed at %s, please install from %s\n' % (blat, ucsc_executables))
            return 1
    except NameError:
        sys.stderr.write('blat is not locally installed at %s, please install from %s\n' % (blat,ucsc_executables))
        return 1
        #sys.exit(1)

def gene_name_to_coordinate_conversion(gene_file,gene_dictionary,outdir):
    """converts gene name to genomic coordinate in the form of a write out to text file

    Input:
    gene_file: absolute filepath to single column text file with gene names, with one gene name per line
    gene_dictionary: output object of gene_dictionary_from_bed_file()
    outdir: absolute filepath to output file

    Output:
    out: absolute filepath to genomic coordinates for gene names queried by user

    """
    with open(gene_file,'r') as infile:
        #wd = os.getcwd() #software implementation
        wd = outdir # website implementation
        out = '%s/Upload2.txt' % (wd)
        count,query = 0,0
        with open(out, 'w') as outfile:
            for line in infile:
                clean_line = line.lstrip().rstrip()
                parts = clean_line.split()
                try:
                    if gene_dictionary.has_key(parts[0]):
                        for coord in gene_dictionary[parts[0]]:
                            outfile.write('%s\n' % coord)
                        count += 1
                        query += 1
                    elif re.match(r"^chr\w+:\d+-\d+$", parts[0]) or re.match(r"^chr\d+:\d+-\d+$", parts[0]):
                        outfile.write('%s \n' % (parts[0]))
                        count += 1
                        query += 1
                    else:
                        query += 1
                        sys.stdout.write('%s is not an identified element in the dictionary, skipping \n' % parts[0])
                        continue
                except:
                    sys.stderr.write('WARNING: unable to process %s: skipping\n' % (clean_line))
                    continue

        sys.stdout.write('gene name to coordinate conversion complete for %s of %s queries \n' % (count,query))
        return out

def gene_dictionary_from_bed_file(bed_file):
    """generates a gene dictionary object where gene name is key and coordinates are values

    Input:
    bed_file: absolute filepath to a bed file with gene annotations

    Output:
    gene_dictionary: dictionary object with gene name as key and genomic coordinate as value

    """
    gene_dictionary = defaultdict(list)
    with open(bed_file,'r') as bed:
        for line in bed:
            clean_line = line.lstrip().rstrip()
            parts = clean_line.split()
            gene_dictionary[parts[3]].append('%s:%s-%s' % (parts[0],parts[1],parts[2]))

    sys.stdout.write('gene dictionary for %s generated \n' % (bed_file))
    return gene_dictionary

def input_file_format_identfier(input_file,gene_dictionary):
    """Verifies input file is an acceptable format that can be processed by batch_query

    Input:
    input_file: absolute filepath of the .bed, .gtf, or .txt file containing only genomic coordinates to be processed
    gene_dictionary: output object of gene_dictionary_from_bed_file()

    Convention:
    The function will check if the file is first a txt file. It will look to ensure only only field (column) is present
    with one genomic coordinate per line. The genomic coordinates are expected to take the form chrQ:start-end where
    Q can be either a number of letter. If only one field exists but the data is not in genomic coordinate format then
    the function rejects the file. Otherwise if only one field exists and it has genomic coordinates it is processed.
    If more than one field exists then the first field is checked to ensure it has chr at the start of every string.
    BED files are required to have chromosome information in the first field. GTF files do not require this, but often
    have the first field have chromosome information. If the first field does not have chromosome information then the
    file is rejected. However, if the first field has chromosome information then the file is checked for start and end
    coordinates. In BED files the second and third field respectively have this information. Provided end coordinates
    are always > start coordinates the file will be accepted and processed. In GTF files the fourth and fifth field have
    start and end coordinates respectively. Provided end coordinates are always > start coordinates the file will be
    accepted and processed. If end coordinates !> start coordinates then the file will be rejected. Files do NOT need to
    have .txt, .bed, .gtf extensions to be processed, but every line in the file must conform to .txt, .bed, .gtf format.
    Therefore it is appropiate to strip any headers from the input file before giving it to the package

    """
    with open(input_file) as batch_file:
        batch_file_format = 'reject'
        fasta_file_check = batch_file.readline()
        if '>' in fasta_file_check[0]:
            batch_file_format = 'fasta'
        else:
            for line in batch_file:
                if line != '\n':
                    clean_line = line.lstrip().rstrip()
                    parts = clean_line.split()
                    if len(parts) == 1:
                        if re.match(r"^chr\w+:\d+-\d+$", parts[0]) or re.match(r"^chr\d+:\d+-\d+$", parts[0]):
                            batch_file_format = 'txt'
                        elif gene_dictionary.has_key(parts[0]):
                            batch_file_format = 'txt'
                        else:
                            sys.stderr.write('ERROR: %s is not a permitted file format, ensure only column in .txt file has genomic '
                                             'coordinates of format chr#:start-end \n')
                            batch_file_format = 'reject'
                            break
                            #sys.exit(1)
                    else:
                        if 'chr' or 'CHR' in parts[0] and len(parts) >= 3:
                            try:
                                parts[1], parts[2] = int(parts[1]), int(parts[2])
                                if int(parts[2]) - int(parts[1]) > 0:
                                    batch_file_format = 'bed'
                                else:
                                    sys.stderr.write(
                                        'ERROR: %s is not a permitted file format, ensure 2nd and 3rd fields are start and'
                                        ' end coordinates respectively for a .bed file \n'
                                        % (batch_file))
                                    batch_file_format = 'reject'
                                    break
                                    #sys.exit(1)

                            except ValueError:
                                try:
                                    parts[3], parts[4] = int(parts[3]), int(parts[4])
                                    if int(parts[4]) - int(parts[3]):
                                        batch_file_format = 'gtf'
                                except IndexError:
                                    sys.stderr.write(
                                        'ERROR: %s is not a permitted file format, ensure 3rd and 4th fields are start and'
                                        ' end coordinates respectively for a .gtf file \n' % (input_file))
                                    batch_file_format = 'reject'
                                    break
                                    #sys.exit(1)
                                except:
                                    sys.stderr.write('ERROR: %s is not a permitted file format \n' % (input_file))
                                    batch_file_format = 'reject'
                                    break

                        else:
                            sys.stderr.write('ERROR: %s is not a permitted file format, please enter .bed, .gtf (with first field a '
                                             'chromosome name like chr:#), or .txt file with genomic coordinates' % (input_file))
                            batch_file_format = 'reject'
                            break
                            #sys.exit(1)
                else:
                    sys.stderr.write('WARNING: newline character encountered in %s \n' % (input_file))
                    continue

    return batch_file_format

def organism_chromosome_identity_and_length_confirmation(bam_file):
    verification_dict = {}
    bamfile = pysam.AlignmentFile(bam_file,'rb')
    chrom_data = bamfile.header
    for i in range(len(chrom_data['SQ'])):
        verification_dict[chrom_data['SQ'][i]['SN']] = chrom_data['SQ'][i]['LN']
    bamfile.close()
    return verification_dict

def noBodyProcess():
    """direct control of file upload destination"""
    cherrypy.request.process_request_body = False

def expose_as_csv(f):
    """
    awesome trick to expose data as csv

    taken from:
    http://code.activestate.com/recipes/573461-publish-list-data-as-csv-file/
    """

    @cherrypy.expose()
    #@strongly_expire
    def wrap(*args, **kw):
        rows = f(*args, **kw)
        out = StringIO.StringIO()
        writer = csv.writer(out) 
        writer.writerows(rows)
        cherrypy.response.headers["Content-Type"] = "application/csv" #invokes download
        cherrypy.response.headers["Content-Length"] = out.len
        return out.getvalue()
    return wrap

def secureheaders():
    headers = cherrypy.response.headers
    headers['X-Frame-Options'] = 'DENY'
    headers['X-XSS-Protection'] = '1; mode=block'
    headers['Content-Security-Policy'] = "default-src='self'"

def clean_on_end():
    print "Cleaning.."
    del crispr
    print "Cleaned."

def midnightloghandler(fn):
    h = TimedRotatingFileHandler(fn, "midnight")
    h.setLevel(logging.DEBUG)
    h.setFormatter(cherrypy._cplogging.logfmt)
    return h

#################################
#                               #
#   Annotation Interval Trees   #
#                               #
#################################

SUPPORTED_ANNOTS = ['hg38', 'mm10', 'dm6', 'sacCer3','danRer10','ce11']
def load_annots():
    annots = {}
    for tag in SUPPORTED_ANNOTS:
        annots[tag] = util.create_annot_inttree(tag)
    return annots
ANNOTS = load_annots()

#########################
#                       #
#   BLAT Local Path     #
#                       #
#########################

blat = '/guidescan/crispr-project/guidescan-crispr/guidescan/blat_binaries/linux.x86_64/blat/blat' #Amazon
#blat = '/Users/pereza1/Tools//blat' # Alex local

#############################
#                           #
#   Organism Fasta Files    #
#                           #
#############################

"""
# Alex local files
hg38_fasta = '/Users/pereza1/Reference_Genomes/hg38/hg38.fa' #Alex local
mm10_fasta = '/Users/pereza1/Reference_Genomes/mm10/mm10.fa' # Alex local
dm6_fasta = '/Users/pereza1/Reference_Genomes/dm6/dm6.fa' #Alex local
"""

# Amazon fasta files
hg38_fasta = '/guidescan/crispr-project/guidescan-website/fasta/hg38/hg38.fa'
mm10_fasta = '/guidescan/crispr-project/guidescan-website/fasta/mm10/mm10.fa'
dm6_fasta = '/guidescan/crispr-project/guidescan-website/fasta/dm6/dm6.fa'
danRer10_fasta = '/guidescan/crispr-project/guidescan-website/fasta/danRer10/danRer10.fa'
ce11_fasta = '/guidescan/crispr-project/guidescan-website/fasta/ce11/ce11.fa'
SacCerv_fasta = '/guidescan/crispr-project/guidescan-website/fasta/SacCerv/SacCerv.fa'

##############################
#                            #
#   Fasta File Dictionaries  #
#                            #
##############################

fasta_dictionary = {'dm6':dm6_fasta,'hg38':hg38_fasta,'mm10':mm10_fasta,'danRer10':danRer10_fasta,'ce11':ce11_fasta,
                    'SacCerv':SacCerv_fasta}

#############################
#                           #
#   Gene Symbol BED Files   #
#                           #
#############################

"""
# Alex local files
hg38_bed_file = '/Users/pereza1/Reference_Genomes/gene_body_annotations/beds/hg38/hg38_annotation.bed' #Alex local
mm10_bed_file = '/Users/pereza1/Reference_Genomes/gene_body_annotations/beds/mm10/mm10_annotation.bed' #Alex local
dm6_bed_file = '/Users/pereza1/Reference_Genomes/gene_body_annotations/beds/dm6/dm6_gene_body_annotation.bed' #Alex local
danRer10_bed_file = '/Users/pereza1/Reference_Genomes/gene_body_annotations/beds/danRer10/danRer10_gene_body_annotation.bed' #Alex local
ce11_bed_file = '/Users/pereza1/Reference_Genomes/gene_body_annotations/beds/ce11/ce11_gene_body_annotation.bed' #Alex local
SacCerv_bed_file = '/Users/pereza1/Reference_Genomes/gene_body_annotations/beds/SacCerv/SacCerv_gene_body_annotation.bed' #Alex local
"""

# Amazon gene body BED files
hg38_bed_file = '/guidescan/crispr-project/guidescan-website/gene_body_annotations/hg38/hg38_annotation.bed'
mm10_bed_file = '/guidescan/crispr-project/guidescan-website/gene_body_annotations/mm10/mm10_annotation.bed'
dm6_bed_file = '/guidescan/crispr-project/guidescan-website/gene_body_annotations/dm6/dm6_gene_body_annotation.bed'
danRer10_bed_file = '/guidescan/crispr-project/guidescan-website/gene_body_annotations/danRer10/danRer10_gene_body_annotation.bed'
ce11_bed_file = '/guidescan/crispr-project/guidescan-website/gene_body_annotations/ce11/ce11_gene_body_annotation.bed'
SacCerv_bed_file = '/guidescan/crispr-project/guidescan-website/gene_body_annotations/SacCerv/SacCerv_gene_body_annotation.bed'

#################################
#                               #
#   Gene Symbol Dictionaries    #
#                               #
#################################

hg38_gene_dictionary = gene_dictionary_from_bed_file(hg38_bed_file)
mm10_gene_dictionary = gene_dictionary_from_bed_file(mm10_bed_file)
dm6_gene_dictionary = gene_dictionary_from_bed_file(dm6_bed_file)
danRer10_gene_dictionary = gene_dictionary_from_bed_file(danRer10_bed_file)
ce11_gene_dictionary = gene_dictionary_from_bed_file(ce11_bed_file)
SacCerv_gene_dictionary = gene_dictionary_from_bed_file(SacCerv_bed_file)

gene_dictionary = {'dm6':dm6_gene_dictionary,'hg38':hg38_gene_dictionary,'mm10':mm10_gene_dictionary,
                   'danRer10':danRer10_gene_dictionary,'ce11':ce11_gene_dictionary,'SacCerv':SacCerv_gene_dictionary}

#############################
#                           #
#   Database Directories    #
#                           #
#############################

"""
BAMDATA = {'dm6': '../testing-data/website-test-bam/dm6_website_chr4_310000-320000.bam',
           'hg38': '../testing-data/website-test-bam/hg38_website_chr4_310000_320000.bam',
           'mm10': '/Users/pereza1/Projects/Ventura/CRISPR/data/mm10/mm10_database.bam',
           }
"""

"""
#MSK virtual machine
BAMDATA = {'dm6': '/data/dm6/dm6_database.bam',
           'mm10': '/data/mm10/mm10_database.bam',
           'hg38': '/data/hg38/hg38_database.bam',
           }
"""


#amazon virtual machine
BAMDATA = {'dm6_cas9': '/guidescan/databases/dm6/cas9/cas9_dm6_all_guides.bam',
           'mm10_cas9': '/guidescan/databases/mm10/cas9/cas9_mm10_all_guides.bam',
           'hg38_cas9': '/guidescan/databases/hg38/cas9/cas9_hg38_all_guides.bam',
           'ce11_cas9': '/guidescan/databases/ce11/cas9/cas9_ce11_all_guides.bam',
           'danRer10_cas9': '/guidescan/databases/danRer10/cas9/cas9_danRer10_guides.bam',
           'SacCerv_cas9':'/guidescan/databases/SacCerv/cas9/cas9_SacCerv_all_guides.bam',
           'dm6_cpf1': '/guidescan/databases/dm6/cpf1/cpf1_dm6_guides.bam',
           'mm10_cpf1': '/guidescan/databases/mm10/cpf1/cpf1_mm10_guides.bam',
           'hg38_cpf1': '/guidescan/databases/hg38/cpf1/cpf1_hg38_guides.bam',
           'ce11_cpf1': '/guidescan/databases/ce11/cpf1/cpf1_ce11_guides.bam',
           'danRer10_cpf1':'/guidescan/databases/danRer10/cpf1/cpf1_danRer10_guides.bam',
           'SacCerv_cpf1':'/guidescan/databases/SacCerv/cpf1/cpf1_SacCerv_guides.bam'
           }


"""
#Sagar ISKI
BAMDATA = {'dm6_cas9': '../testing-data/website-test-bam/dm6_website_chr4_310000-320000.bam',
          'mm10_cas9':'../testing-data/website-test-bam/mm10_website_chr4_3100000_3200000.bam',
          'hg38_cas9':'../testing-data/website-test-bam/hg38_website_chr4_310000_320000.bam',
          'yeast_cas9':'../testing-data/website-test-bam/yeast_cas9_all.bam',
          'yeast_cpf1':'../testing-data/website-test-bam/yeast_cpf1_all_guides.bam',
          'dm6_cpf1':'../testing-data/cpf1/dm6_cpf1/cpf1_dm6_guides.bam',
          'ce11_cpf1':'../testing-data/cpf1/dm6_cpf1/cpf1_ce11_guides.bam',
          'danRer10_cpf1':'../testing-data/cpf1/dm6_cpf1/cpf1_danRer10_guides.bam',
          'SacCerv_cpf1':'../testing-data/cpf1/dm6_cpf1/cpf1_SacCerv_guides.bam'}
"""

"""
#Alex local
BAMDATA = {'dm6_cas9': '/Users/pereza1/Projects/Ventura/CRISPR/data/dm6/site_download/cas9_dm6_all_guides.bam',
          'mm10_cas9':'/Users/pereza1/Projects/Ventura/CRISPR/data/mm10/site_download/cas9_mm10_all_guides.bam',
          'hg38_cas9':'/Users/pereza1/Projects/Ventura/CRISPR/data/hg38/site_download/cas9_hg38_all_guides.bam'}
"""

#####################################################
#                                                   #
#   Organism Chromosome Identifications and Length  #
#                                                   #
#####################################################

dm6_cas9_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['dm6_cas9'])
mm10_cas9_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['mm10_cas9'])
hg38_cas9_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['hg38_cas9'])
ce11_cas9_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['ce11_cas9'])
SacCerv_cas9_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['SacCerv_cas9'])
danRer10_cas9_dict= organism_chromosome_identity_and_length_confirmation(BAMDATA['danRer10_cas9'])

dm6_cpf1_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['dm6_cpf1'])
mm10_cpf1_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['mm10_cpf1'])
hg38_cpf1_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['hg38_cpf1'])
ce11_cpf1_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['ce11_cpf1'])
SacCerv_cpf1_dict = organism_chromosome_identity_and_length_confirmation(BAMDATA['SacCerv_cpf1'])
danRer10_cpf1_dict= organism_chromosome_identity_and_length_confirmation(BAMDATA['danRer10_cpf1'])

#################
#               #
#   Classes     #
#               #
#################

class Crispr:
    def __init__(self,queries,genometag,mode,flank_size,ordering,display,datafile,topN,enzyme,tmpdir='',tooManyQueries=False):
        """Main class to process user input and prepare results to show.

        queries: (str) user input in query text area; supposedly contains
                 one or multiple genomic region coordinates to query
                 the database;
                 TODO: allow gene names as queries here
        genometag: str with short name of genome (e.g., "hg38")
        mode: "flanking" or "within"
        flank_size: (int) size of flanking window (for mode "flanking")
        ordering: "coordinates" or "offtargets" or "score",
                  determines how output guideRNAs are sorted
        """
        self.queries = queries 
        self.genometag = genometag
        self.enzyme = enzyme
        # name of BAM file with guideRNA database
        self.bamdata = BAMDATA.get(self.genometag+"_"+self.enzyme)

        self.mode = mode
        self.error = []      # where all errors will be stored
        self.warning = []    # where all warning will be stored
        self.results = []    # list of pairs
                             # (query, mode, bedlines of guideRNAs)
                             # where mode is "within" or "upstream"
                             # or "downstream"
        self.flank_size = flank_size
        self.ordering = ordering
        self.display = display
        self.topN = topN
        self.offtargets = defaultdict(list)  # dict of the form
                              # {sequence of guideRNA + PAM :
                              #  bedlines of offtargets for this guideRNA})
                              # with all off-targets across all queries
                              # combined
        self.datafile = datafile
        self.tmpdir = tmpdir
        self.tooManyQueries = False

        self.line_counter = 0
        self.guides_regions = 0
        self.line_counter = 0 
        self.guides_regions = 0

    def calculate(self):
        # TODO: restructure all error messsages
        # TODO: add check that self.bamdata is present and exists

        queries = self.process_input(self.queries)

        size_restriction = 2500000
        maxoutput=500

        #ensure flanking size is not absurd value
        if self.flank_size:
            try:
                if int(self.flank_size) > size_restriction:
                    self.flank_size = str(size_restriction)
                else:
                    pass
            except:
                self.flank_size = str(1000)
        else:
            sys.stderr.write('user entered nothing into flank /n')
            self.flank_size = str(1000)

        gRNA_reported_per_query = size_restriction

        #If file upload
        if self.datafile:
          print("found datafile!")

          #strip \r characters that may be present in file
          carriage_strip_cmd = '%s %s' % ("perl -pi -e '\s/\r/\n/g'",self.datafile)
          os.system(carriage_strip_cmd)

          #identify file format or reject
          process_signal = input_file_format_identfier(self.datafile,gene_dictionary[self.genometag])

          #assess if file is fasta file and convert sequence to coordinate(s) file
          if process_signal == 'fasta':
              outfile = blat_processing(blat, fasta_dictionary[self.genometag], self.datafile, self.tmpdir, verbose=True)
              #print outfile
              os.remove(self.tmpdir + "/Upload.txt")
              os.rename(outfile, self.tmpdir + "/Upload.txt")
              sys.stdout.write('uploaded file %s contains .fa ending, converting to coordinates\n' % outfile)
              try:
                  blat_coordinate_file_exist = self.tmpdir + "/Upload.txt"
                  with open(blat_coordinate_file_exist,'r') as exist_test:
                      existence = exist_test.readline()
                      if existence:
                        sys.stdout.write('existence test passed with %s\n' % existence)
                      else:
                          sys.stderr.write('existence test failed; reject file\n')
                          process_signal = 'reject'
              except:
                  sys.stderr.write('existence test failed; reject file\n')
                  process_signal = 'reject'

          #assess if txt file has gene symbols, if they exist convert to coordinates
          if process_signal == 'txt':
              gene_file_new = gene_name_to_coordinate_conversion(self.datafile, gene_dictionary[self.genometag], self.tmpdir)
              os.remove(self.tmpdir + "/Upload.txt")
              os.rename(gene_file_new,self.tmpdir + "/Upload.txt")
              sys.stdout.write('gene symbols identified in uploaded file %s, converting to coordinates\n' % gene_file_new)

          if process_signal == 'reject':
              self.error.append(["ERROR","Uploaded file format not recognized, ensure file is .bed, .gtf, fasta, or .txt with one column composed of genomic coordinates"])
              return
          else:
              if self.display == 'top3':
                  if self.ordering == 'score':
                      select_on = 'score'
                  elif self.ordering == 'offtargets':
                      select_on = 'offtargets'
                  elif self.ordering == 'coordinates':
                      select_on = 'coordinates'
                  elif self.ordering == 'specificity':
                      select_on = 'specificity'
                  else:
                      select_on = ''
              else:
                  select_on = ''

          annot_a = ANNOTS.get(self.genometag)

          if self.mode == 'within':
              guides_within, query_lst, self.line_counter, self.guides_regions = guidequery.batch_query(
                  input_file=self.tmpdir + "/Upload.txt", output_dir=self.tmpdir,
                  bam_file=self.bamdata, target=self.mode,
                  flankdistance=self.flank_size, select=select_on,
                  sort=self.ordering, n=self.topN, formating='csv', off=False,annot=annot_a, bedlines=True)

          elif self.mode == 'flanking':
              guides_left,guides_right,query_lst,self.line_counter,self.guides_regions = \
                    guidequery.batch_query(input_file=self.tmpdir + "/Upload.txt",output_dir=self.tmpdir,
                                           bam_file=self.bamdata,target=self.mode,flankdistance=self.flank_size,
                                           select=select_on,sort=self.ordering, n=self.topN,formating='csv',off=False,
                                           bedlines=True,annot=annot_a)

          """
              guidequery.batch_query(input_file=self.datafile,output_dir=self.tmpdir,bam_file=self.bamdata,target=self.mode,
                                     flankdistance=self.flank_size,select=select_on,sort=self.ordering,n=self.topN,
                                     formating='csv',off=False)
            """

        ###############################
        #or continue with normal query#
        ###############################
        else:
 
            ###############
            #error catches#
            ###############
            error=False
            if len(queries) > 1000:
              self.error.append(["ERROR",
                                 "More than 1000 entries. Please input less than 1000 entries."])
              return

            for gene_query in range(len(queries)):
                if gene_dictionary[self.genometag].has_key(queries[gene_query]):
                    gene_key = queries[gene_query]
                    #print gene_key,gene_query,queries
                    queries.pop(gene_query)
                    for gene_coord in gene_dictionary[self.genometag][gene_key]:
                        queries.append(gene_coord)
                else:
                    continue

            for i in range(len(queries)):
                query_type = self.getType(queries[i])
 
                if query_type=="unknown":
                    self.error.append(["ERROR",
                                       queries[i] + ": Input type could not be identified. Potential newline characters "
                                                    "in textbox. Please ensure that you entered a valid coordinate in "
                                                    "format: chr4:310000-320000"])
                    error=True

            if self.mode != "within" and not self.flank_size.isdigit():
              self.error.append(["ERROR",
                                "Please enter a positive integer in the \"Flanking\" field."])
              error=True

            if self.mode != "within" and self.flank_size.isdigit():
              if int(self.flank_size) <= 0:
                self.error.append(["ERROR",
                                "Please enter a positive integer greater than 0 in the \"Flanking\" field."])
                error=True

            if self.display != "all" and not self.topN.isdigit():
              self.error.append(["ERROR",
                                "Please enter a positive integer in the \"Results Display\" field."])
              error=True

            if self.display != "all" and self.topN.isdigit():
              if int(self.topN) <= 0:
                self.error.append(["ERROR",
                                "Please enter a positive integer greater than 0 in the \"Results Display\" field."])
                error=True

            if self.enzyme == 'cpf1' and (self.ordering == "specificity" or self.ordering == "score"):
              self.error.append(["ERROR","Specificity and score ordering are only valid for cas9"])
              error=True

            if error == True:
              return

            ####################################
            #ensure organism and query match up#
            ####################################

            if str(self.genometag) == 'dm6' and str(self.enzyme) == 'cas9':
                genome_verify = dm6_cas9_dict
            elif str(self.genometag) == 'mm10' and str(self.enzyme) == 'cas9':
                genome_verify = mm10_cas9_dict
            elif str(self.genometag) == 'SacCerv' and str(self.enzyme) == 'cas9':
                genome_verify = SacCerv_cas9_dict
            elif str(self.genometag) == 'ce11' and str(self.enzyme) == 'cas9':
                genome_verify = ce11_cas9_dict
            elif str(self.genometag) == 'dm6' and str(self.enzyme) == 'cpf1':
                genome_verify = dm6_cpf1_dict
            elif str(self.genometag) == 'SacCerv' and str(self.enzyme) == 'cpf1':
                genome_verify = SacCerv_cpf1_dict
            elif str(self.genometag) == 'ce11' and str(self.enzyme) == 'cpf1':
                genome_verify = ce11_cpf1_dict
            elif str(self.genometag) == 'danRer10' and str(self.enzyme) == 'cpf1':
                genome_verify = danRer10_cpf1_dict
            elif str(self.genometag) == 'hg38' and str(self.enzyme) == 'cpf1':
                genome_verify = hg38_cpf1_dict
            elif str(self.genometag) == 'danRer10' and str(self.enzyme) == 'cas9':
                genome_verify = danRer10_cas9_dict
            elif str(self.genometag) == 'mm10' and str(self.enzyme) == 'cpf1':
                genome_verify = mm10_cpf1_dict
            else:
                genome_verify = hg38_cas9_dict

            if error==True:
              return

            ########################################
            #batch_query writeout for textbox input#
            ########################################
            df=open(self.tmpdir+"/Upload.txt","w")
            print(self.tmpdir)
            valid_queries=0
            for j in range(len(queries)):
                if j != '\n':
                    chrom,start,end = str(queries[j].split(':')[0]),int(queries[j].split(':')[1].split('-')[0]),\
                                      int(queries[j].split(':')[1].split('-')[1])
                    if start != end:
                        if end > start:
                            #if query greater than size and within query, change the coordinates 
                            if (end - start) > size_restriction and self.mode=="within":
                                end = start + size_restriction #per Andrea's request, if above restriction, query start-restriction                                                                                                                                                                                                                                     
                                self.warning.append(["WARNING",
                                                     "Query on region greater than %s, first %s bases of query chosen, new query region %s:%s-%s" %
                                                     (size_restriction, size_restriction,chrom, start, end)])

                            if genome_verify.has_key(chrom):
                                if start <= genome_verify[chrom] and end <= genome_verify[chrom]:
                                    df.write('%s \n' % (queries[j]))
                                    valid_queries+=1
                                else:
                                    self.warning.append(["ERROR","Coordinate %s:%s-%s does not exist for %s: %s has max length of %s"
                                                         % (chrom,start,end,str(self.genometag),chrom,genome_verify[chrom])])
                            else:
                                self.error.append(["ERROR","%s is not a chromosome in %s" %
                                                   (chrom,str(self.genometag))])
                                return
                        else:
                            self.warning.append(["ERROR","End coordinate greater than start coordinate"])
                    else:
                        self.warning.append(["ERROR","start coordinate %s equals end coordinate %s in %s:%s-%s" %
                                             (start,end,chrom,start,end)])
                else:
                    continue
            df.close()

            if valid_queries == 0:
                return
                                

            # if os.path.exists(self.tmpdir):
            #     print('self.tmpdir is %s' % (self.tmpdir))
            #     print(self.tmpdir,df.name,type(self.tmpdir),type(df.name))

            #################################
            #engage auto-selection of sgRNAs#
            #################################
            if self.display == 'top3':
                if self.ordering == 'score':
                    select_on = 'score'
                elif self.ordering == 'offtargets':
                    select_on = 'offtargets'
                elif self.ordering == 'coordinates':
                    select_on = 'coordinates'
                elif self.ordering == 'specificity':
                    select_on = 'specificity'
                else:
                    select_on = ''
            else:
                select_on = ''

            if self.mode == "within":

                annot_a = ANNOTS.get(self.genometag)

                guides_within,query_lst,guides_within_OT,self.line_counter,self.guides_regions = guidequery.batch_query(
                    input_file=self.tmpdir + "/Upload.txt", output_dir=self.tmpdir,
                    bam_file=self.bamdata, target=self.mode,
                    flankdistance=self.flank_size, select=select_on,
                    sort=self.ordering, n=self.topN, formating='csv', off=True, annot=annot_a, bedlines=True)

                OT_dict = defaultdict(list)
                query_count = 0

                for guides in guides_within_OT: #guides_within
                    query = query_lst[query_count]

                    #############################################
                    #strip PAM from target sequence to make gRNA#
                    #############################################
                    gRNA_lst = []
                    out_count = 0

                    for y in guides:
                        if out_count <= gRNA_reported_per_query and out_count <= maxoutput :
                            y = list(y)
                            if y[10] == 0:
                                y[3] = y[3].replace('NGG',' NGG')
                                y = tuple(y)
                                gRNA_lst.append(y)
                                out_count += 1
                            else:
                                y = tuple(y)
                                gRNA_lst.append(y)
                        else:
                            break

                    ################################
                    #annotation interval tree query#
                    ################################
                    annots = ANNOTS.get(self.genometag)
                    if annots:
                        gRNA_lst = guidequery.annotate_bed(gRNA_lst, annots)
                    else:
                        pass

                    #####################
                    #output within gRNAs#
                    #####################
                    res_temp=[b for b in gRNA_lst if b[10] == 0]
                    self.results.append((query, "within",
                                         res_temp,len([g for g in guides if g[10]==0])))
                    del(res_temp)
#                    print(gRNA_lst[0], 'check')
#                    print(self.results)
                    #######################
                    #offtarget information#
                    #######################
                    for b in gRNA_lst:
                        if b[10] != 0:
                            coordinate_confirm = ('%s:%s-%s' % (b[0],b[1],b[2]))
                            if OT_dict[coordinate_confirm]:
                                continue
                            else:
                                OT_dict[coordinate_confirm].append('Y')
                                self.offtargets[b[3].replace('NGG',' NGG')].append(b)

                    query_count += 1

                #del(guides_within,query_lst,guides_within_OT)

            elif self.mode == "flanking":

                annot_a = ANNOTS.get(self.genometag)

                guides_left,guides_right,query_lst,guides_left_OT,guides_right_OT,self.line_counter,self.guides_regions = \
                    guidequery.batch_query(input_file=self.tmpdir + "/Upload.txt",output_dir=self.tmpdir,
                                           bam_file=self.bamdata,target=self.mode,flankdistance=self.flank_size,
                                           select=select_on,sort=self.ordering, n=self.topN,formating='csv',off=True,annot=annot_a,
                                           bedlines=True)

                OT_dict = defaultdict(list)
                query_count_left,query_count_right = 0,0
                for C in itertools.izip_longest(guides_left_OT,guides_right_OT): #guides_left,guides_right
                    if C[0]:
                        #left_guides = guidequery.get_bed_lines(C[0], offtargets=True)

                        #############################################
                        #strip PAM from target sequence to make gRNA#
                        #############################################
                        left_gRNA_lst = []
                        out_count_left = 0
                        for left_y in C[0]:
                            if out_count_left <= gRNA_reported_per_query  and out_count_left <= maxoutput :
                                left_y = list(left_y)
                                if left_y[10] == 0:
                                    left_y[3] = left_y[3].replace('NGG', ' NGG')
                                    left_y = tuple(left_y)
                                    left_gRNA_lst.append(left_y)
                                    out_count_left += 1
                                else:
                                    left_y = tuple(left_y)
                                    left_gRNA_lst.append(left_y)
                            else:
                                break

                        left_guides = left_gRNA_lst #C[0]
                        query_left = query_lst[query_count_left]
                        query_count_left += 1

                    else:
                        left_guides = []
                        query_left = query_lst[query_count_left]
                        query_count_left += 1

                    if C[1]:
                        #right_guides = guidequery.get_bed_lines(C[1], offtargets=True)

                        #############################################
                        #strip PAM from target sequence to make gRNA#
                        #############################################
                        right_gRNA_lst = []
                        out_count_right = 0
                        for right_y in C[1]:
                            if out_count_right <= gRNA_reported_per_query  and out_count_right <= maxoutput :
                                right_y = list(right_y)
                                if right_y[10] == 0:
                                    right_y[3] = right_y[3].replace('NGG', ' NGG')
                                    right_y = tuple(right_y)
                                    right_gRNA_lst.append(right_y)
                                    out_count_right += 1
                                else:
                                    right_y = tuple(right_y)
                                    right_gRNA_lst.append(right_y)
                            else:
                                break

                        right_guides = right_gRNA_lst #C[1]
                        query_right = query_lst[query_count_right]
                        query_count_right += 1

                    else:
                        right_guides = []
                        query_right = query_lst[query_count_right]
                        query_count_right += 1

                    ################################
                    #annotation interval tree query#
                    ################################
                    annots = ANNOTS.get(self.genometag)
                    if annots:
                        if left_guides and right_guides:
                            left_guides, right_guides = guidequery.annotate_bed(left_guides, annots), \
                                                   guidequery.annotate_bed(right_guides, annots)
                        elif left_guides:
                            left_guides = guidequery.annotate_bed(left_guides, annots)
                        elif right_guides:
                            right_guides = guidequery.annotate_bed(right_guides, annots)
                        else:
                            pass

                    #######################
                    #output flanking gRNAs#
                    #######################
                    res_temp=[b for b in left_guides if b[10] == 0]
                    self.results.append((query_left, "upstream",
                                         res_temp[0:maxoutput],len([g for g in C[0] if g[10]==0])))
                    del res_temp

                    res_temp=[b for b in right_guides if b[10] == 0]
                    self.results.append((query_right, "downstream",
                                         res_temp[0:maxoutput],len([g for g in C[1] if g[10]==0])))
                    del res_temp

                    #######################
                    #offtarget information#
                    #######################
                    guides_flank = [left_guides, right_guides]
                    for j in guides_flank:
                        for b in j:
                            if b[10] != 0:
                                coordinate_confirm = ('%s:%s-%s' % (b[0], b[1], b[2]))
                                if OT_dict[coordinate_confirm]:
                                    continue
                                else:
                                    OT_dict[coordinate_confirm].append('Y')
                                    self.offtargets[b[3].replace('NGG',' NGG')].append(b)

            #os.remove(df.name)
            #del(guides_left,guides_right,query_lst,guides_left_OT,guides_right_OT)

    def process_input(self, inputs):
        splitInput = inputs.splitlines()
        splitInput = [s.strip() for s in splitInput]
        return splitInput
       
    def getType(self, seq):
        seqType = None
        if re.match("^chr[0-9|XYMIV]+:[0-9]+-[0-9]+$", seq) is not None:
            seqType = "ezseq"
        elif re.match("^chr[[23][LR]|4XYM]+:[0-9]+-[0-9]+$",seq) is not None:
            seqType = "ezseq"
        else:
            seqType="unknown"

        return seqType
    
class TemporaryFileStorage(cherrypy._cpreqbody.Part):
    """custom entity part that will always create a named temporary file for the entities (stackoverflow)"""
    maxrambytes = 0

    def make_file(self):
        return tempfile.NamedTemporaryFile()

class Root:
    def __init__(self):
        print('Loading Site...')

    @cherrypy.expose
    def index(self, **params):
        """
        set up site
        """

        tmpl = env.get_template('crispr.html')
        queries = "chr4:310000-320000\n"
        self.kmer = "20"
        self.genome = "hg38"
        self.mode="within"
        self.flank_size="1000"
        self.ordering='offtargets'
        self.display='all'
        self.datafile=''
        self.topN="3"
        self.tooManyQueries=False
        self.enzyme="cas9"
        
        acceptable_characters = string.ascii_letters + string.digits + string.whitespace + '#:-_.*'
        queries = ''.join(character for character in queries if character in acceptable_characters)
        self.kmer = ''.join(character for character in self.kmer if character in acceptable_characters)
        self.genome = ''.join(character for character in self.genome if character in acceptable_characters)
        self.mode = ''.join(character for character in self.mode if character in acceptable_characters)
        self.flank_size = ''.join(character for character in self.flank_size if character in acceptable_characters)
        self.ordering = ''.join(character for character in self.ordering if character in acceptable_characters)
        self.topN = ''.join(character for character in self.topN if character in acceptable_characters)
        self.display = ''.join(character for character in self.display if character in acceptable_characters)
        self.enzyme = ''.join(character for character in self.enzyme if character in acceptable_characters)

        return tmpl.render(queries=queries, kmer = self.kmer,
                           genome = self.genome,
                           mode=self.mode, flank_size=self.flank_size,
                           ordering=self.ordering, topN=self.topN, display=self.display, datafile=self.datafile,enzyme=self.enzyme)

    @cherrypy.expose
    def show_results(self,datafile=None,queries="", kmer="20", genome="hg38",
                     mode="within", flank_size=1000, ordering='offtargets',topN=3,display='all',enzyme='cas9',
                     **params):

        
        try:
          firstLine=datafile.file.readline()
        except AttributeError:
          firstLine=''

        if mode=="within":
            flank_size=1000
            
        if display=="all":
            topN=3

        if(firstLine==''):
          print("Normal Query")
          tmpl = env.get_template('crispr.html')
          tmpdir=tempfile.mkdtemp()

          crispr = Crispr(queries = queries.replace(',', ''),
                          genometag = genome, mode = mode,
                          flank_size = flank_size, ordering = ordering,display=display,topN=topN,datafile='',tmpdir=tmpdir,tooManyQueries=False,enzyme=enzyme)

          crispr.calculate()
          cherrypy.log(cherrypy.request.remote.ip + "\t" + kmer + "\t" + str(len(crispr.results)) + " results")

          #print(crispr.results)

          gc.collect()

          return tmpl.render(error=crispr.error,warning=crispr.warning,
                             result=crispr.results, offtargets=crispr.offtargets,
                             queries=crispr.queries,
                             kmer=kmer, genome=crispr.genometag,
                             mode=crispr.mode, flank_size=crispr.flank_size,
                             ordering=crispr.ordering,display=crispr.display,
                             line_counter = crispr.line_counter, guides_regions=crispr.guides_regions,
                             enzyme=crispr.enzyme,
                             topN=topN,tmpdir=crispr.tmpdir,tooManyQueries=crispr.tooManyQueries)
        else:
          print("Upload Query")

          datafile.file.seek(0)
          tmpdir=tempfile.mkdtemp()
          dfname=tmpdir + '/Upload.txt'

          df = open(dfname,'w')
          df.write(datafile.file.read())
          df.close()

          num_queries= sum(1 for line in open(dfname) if line!= "\n")

          tmpl = env.get_template('crispr.html')
          crispr = Crispr(queries = '',
                          genometag = genome, mode = mode,
                          flank_size = flank_size,ordering = ordering,topN=topN,display=display,datafile=dfname,tmpdir=tmpdir,enzyme=enzyme)

          crispr.calculate()
          cherrypy.log(cherrypy.request.remote.ip + "\t" + kmer + "\t" + str(len(crispr.results)) + " results")

          if os.path.exists(crispr.datafile):
              print('%s EXISTS! ' % (df.name))

          if os.path.exists(crispr.tmpdir):
              print('crispr.tmpdir is %s' % (crispr.tmpdir))

          if os.path.exists(crispr.tmpdir + '/GuideScan_batch_ontargets_and_offtargets.csv'):
              print('GuideScan_batch_ontargets_and_offtargets.csv')
          else:
              os.listdir(crispr.tmpdir)

          gc.collect()

          return tmpl.render(error=crispr.error, warning=crispr.warning,result=crispr.results, queries=queries, kmer = 20,
                             genome = genome,
                             mode=crispr.mode, flank_size=crispr.flank_size,
                             ordering=crispr.ordering, display=crispr.display,topN=topN, 
                             line_counter = crispr.line_counter, guides_regions=crispr.guides_regions,
                             datafile=crispr.datafile,tmpdir=crispr.tmpdir,tooManyQueries=True,enzyme=crispr.enzyme)
   
    @cherrypy.expose
    def show_offtargets(self, offtargets="", guiderna="", **params):
        tmpl = env.get_template('offtargets.html')
        offtargets = ast.literal_eval(offtargets)
        return tmpl.render(offtargets=offtargets, guiderna=guiderna)

    @cherrypy.expose
    def download_results(self,tmpdir=""):
      file=tmpdir + '/GuideScan_batch_output.csv'
      #file = tmpdir + '/GuideScan_batch_ontargets_and_offtargets.csv'
      serve_file(file,"application/x-download", "attachment")
      dfile=open(file,'r')
      out_file = dfile.read()
      dfile.close()
      return(out_file)
 
    @cherrypy.expose()
    def help(self, **params):
        tmpl_help = env.get_template('help.html')
        return tmpl_help.render()

    @cherrypy.expose()
    def contact(self, **params):
        tmpl_contact = env.get_template('contact.html')
        return tmpl_contact.render()


#####################
#                   #
#   Core Function   #
#                   #
#####################

cherrypy.tools.secureheaders = cherrypy.Tool('before_finalize', secureheaders, priority=60)
cherrypy.tools.noBodyProcess = cherrypy.Tool('before_request_body',noBodyProcess)
cherrypy.tools.clean_on_end = cherrypy.Tool('on_end_resource',clean_on_end)

cherrypy.server.socket_timeout = 3600

#####################
#   Configuration   #
#####################

config = {'/':

                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': current_dir + "/static/",
                 'tools.secureheaders.on': True,
                 'request.body.part_class':TemporaryFileStorage
                },

       'global':    
                { 'server.environment': 'production',
                  'engine.autoreload.on': True,
                  'engine.autoreload.frequency': 5,
                  'server.socket_host': '0.0.0.0',
                  'server.socket_port': 80,
                  'server.thread_pool': 50,
                  'engine.timeout_monitor.on': False,
                  'log.access_file': "",
                  'log.error_file': "",
                  'request.show_tracebacks':False
                },
        }
#VM: 'server.socket_host': 'guidescan.mskcc.org', socket_port = 80,
#test: 'server.socket_host': '127.0.0.1', socket_port = 8080,
#amazon: 'server.socket_host': '0.0.0.0', 'server.socket_port': 80,
#'server.thread_pool': 30, need to talk to Sagar about this

error_dirname = current_dir + "/logs/errorb"
access_dirname = current_dir + "/logs/access"
if not os.path.exists(error_dirname):
    os.makedirs(error_dirname)
if not os.path.exists(access_dirname):
    os.makedirs(access_dirname)

log = cherrypy.log 
log.error_file = ""
log.access_file = "" 

for handler in log.error_log.handlers:
    log.error_log.removeHandler(handler)
for handler in log.access_log.handlers:
    log.access_log.removeHandler(handler)

log.error_log.addHandler(midnightloghandler(error_dirname + "/log_error")) 
log.access_log.addHandler(midnightloghandler(access_dirname + "/log_access")) 

cherrypy.quickstart(Root(), config=config)
