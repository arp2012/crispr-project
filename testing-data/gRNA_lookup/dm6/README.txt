#all gRNAs in database
samtools view cas9_dm6_all_guides.bam | awk '{print $1,$2,$3,$4,$15,$16}' > /cbio/cllab/projects/perez/crispr/sorted_gRNA_text_files/dm6/cas9_dm6_all_guides_lookup.txt

#first 1000000 gRNAs
samtools view cas9_dm6_all_guides.bam | head -n 1000000 | awk '{print $1}' > /cbio/cllab/projects/perez/crispr/sorted_gRNA_text_files/dm6/cas9_dm6_all_guides_lookup_first_1e6.txt
