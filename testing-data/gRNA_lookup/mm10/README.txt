samtools view cas9_dm6_all_guides.bam | head -n 1000000 | cut -c1-20 | awk '{print $1}' > /cbio/cllab/projects/perez/crispr/sorted_gRNA_text_files/mm10/cas9_dm6_all_guides_lookup_first_1e6.txt
